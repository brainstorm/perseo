<?php

namespace PerSeo;

class Router extends \PerSeo\Path
{
	
	protected static $controller;
	
	function __construct() {
		$thisScript = substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], '/')) .'/';
		$purgeURL = ($thisScript != '/' ? str_replace($thisScript, "", $_SERVER['REQUEST_URI']) : substr($_SERVER['REQUEST_URI'], 1));	
		$purgedURL1 = preg_replace("/\?.*$/","", $purgeURL);
		$purgedURL2 = preg_replace("/\&.*$/","", $purgedURL1);
		$finalURL = str_replace("/", "\\", $purgedURL2);
		self::$controller = ($finalURL ? $finalURL : 'Index\Start\main');
		\PerSeo\Request::checkpost();
    }
	
	private static function filterAlpha($string) {
		$unaccent = \PerSeo\Sanitize::unaccent(urldecode($string));
		$replaceSpace = preg_replace('/\s/i','_', $unaccent);
		$finalURL = preg_replace('/[^\da-zA-Z0-9_]/i', '', $replaceSpace);
		return ucfirst(strtolower($finalURL));
	}
	
    public static function routes() {
		try {
		$params = explode("\\", self::$controller);
		$argstmp = urldecode(implode("/", array_slice($params, 3)));
		$args = explode("/", $argstmp);
		$filename = Path::CONF_PATH . Path::DS . 'config.php';
		$ThisModuleName = self::filterAlpha($params[0]);
		$ThisControllerName = (!isset($params[1]) || !$params[1] ? 'Start' : self::filterAlpha($params[1]));
		$ThisMethodName = (!isset($params[2]) || !$params[2] ? 'main' : self::filterAlpha($params[2]));
		if (!file_exists($filename)) { $ThisModuleName = 'Wizard'; }
		else { if (strtolower($ThisModuleName) == 'wizard') { $ThisModuleName = null;} }
		$mylogin = new \PerSeo\Login();
		if ($mylogin->islogged('admins')) {
			if (strtolower($ThisControllerName) == 'admin') {
				$ThisControllerNameTmp = (!isset($params[2]) || !$params[2] ? 'Start' : self::filterAlpha($params[2]));
				$ThisControllerName = 'Admin' . Path::DS . $ThisControllerNameTmp;
				$ThisMethodName = (!isset($params[3]) || !$params[3] ? 'main' : self::filterAlpha($params[3]));
				$argstmp = urldecode(implode("/", array_slice($params, 4)));
				$args = explode("/", $argstmp);
			}
		}
        if(is_readable(Path::MOD_PATH . Path::DS . $ThisModuleName . Path::DS . 'Controllers' . Path::DS . $ThisControllerName .'.php')) {
			Path::$ModuleName = $ThisModuleName;
			Path::$ControllerName = str_replace("/", "\\", $ThisControllerName);
			Path::$ParamsURL = $args;
			Path::$ModPath = realpath(Path::MOD_PATH . Path::DS . Path::$ModuleName);
			Path::$ViewsPath = realpath(Path::MOD_PATH . Path::DS . Path::$ModuleName . Path::DS .'Views');
			Path::$LangPath = realpath(Path::MOD_PATH . Path::DS . Path::$ModuleName . Path::DS .'Languages');
			$controllerName = Path::$ModuleName . '\\Controllers\\' . Path::$ControllerName;
			$methodName = $ThisMethodName;
			$controller = new $controllerName;
			$method = (is_callable(array($controller, $methodName))) ? $methodName : 'main';
			Path::$MethodName = $method;
			call_user_func(array($controller, $method));
        }
		else {
			\PerSeo\PS404::show();
		}	
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
    }

}