<?php

namespace PerSeo;

use Smarty;
use Smarty_Security;

class Template
{	
	protected const PRODNAME = 'PerSeo';
	
	protected const PRODVER = '1.0';
	
	protected const ENCODING = 'utf-8';

	public static function show($dir, $page, $vars=null, $token=false, $caching=false) {
		$_method = new Smarty();
		$_method->enableSecurity();
		$_method->setTemplateDir($dir);
		$_method->setCompileDir(\PerSeo\Path::D_ROOT . \PerSeo\Path::DS .'cache'. \PerSeo\Path::DS .'compile');
		$_method->setCacheDir(\PerSeo\Path::D_ROOT . \PerSeo\Path::DS .'cache'. \PerSeo\Path::DS .'tmp');
		if ($caching) {
			$_method->caching = true;
			$_method->cache_lifetime = $caching;
		}
		else {
			$_method->caching = false;
		}
		if ($vars) {
		foreach ($vars as $key => $value) {
			$_method->assign($key, $value);
		}
		}
		$_method->assign('ProdName', self::PRODNAME);
		$_method->assign('ProdVer', self::PRODVER);
		$_method->assign('host', \PerSeo\Path::MY('HOST'));
		$_method->assign('encoding', self::ENCODING);
		$_method->assign('ModuleName', \PerSeo\Path::ModuleName());
		$_method->assign('lang', \PerSeo\Language::Get());
		//$_method->assign('cookie_path', COOKIE_PATH);
		if ($token) {
			$secure = new \PerSeo\Secure;
			$GetToken = $secure->get_token();
			$_method->assign('CSRFToken', $GetToken);
		}
		$_method->display($page);
	}
}