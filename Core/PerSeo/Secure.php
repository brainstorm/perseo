<?php

namespace PerSeo;

class Secure
{
	public function get_token() {
		return $_SESSION['CSRFGuard'];
	}		
	public function generate_token() {
		$token = (PHP_MAJOR_VERSION >= 7 ? md5(random_bytes(128)) : md5(mcrypt_create_iv(128, MCRYPT_DEV_URANDOM)));
		$_SESSION['CSRFGuard'] = $token;
	}	
	public function init() {
		if (!isset($_SESSION['CSRFGuard'])) {
			$this->generate_token();
		}	
	}	
	public function validate_token($token_value) {
		$token = $this->get_token();
		$result = hash_equals($token, $token_value);
		if ($result) { $this->generate_token(); }
		return $result;
	}
}