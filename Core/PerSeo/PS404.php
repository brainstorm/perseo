<?php

namespace PerSeo;

class PS404
{
	public static function show() {
		header('HTTP/1.0 404 Not Found', true, 404);
		$dir = \PerSeo\Path::D_ROOT . \PerSeo\Path::DS . 'templates';
		$test1 = parse_url(Path::MY('HOST'));
		$vars = array(
			'title' => SITENAME,
			'host' => \PerSeo\Path::MY('HOST')
		);
		\PerSeo\Template::show($dir, '404.tpl', $vars, false, 300);
	}
}