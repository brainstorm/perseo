<?php

namespace PerSeo;

define('DS', DIRECTORY_SEPARATOR);
define('D_ROOT', realpath(__DIR__.DS.'..'.DS.'..'));

class Path
{
	const DS = DS;
	const D_ROOT = D_ROOT;
	const CORE_PATH = D_ROOT.DS.'Core';
	const CONF_PATH = D_ROOT.DS.'config';
	const MOD_PATH = D_ROOT.DS.'modules';
	const LANG_PATH = D_ROOT.DS.'Languages';
	const INC_PATH = D_ROOT.DS.'vendor';
	const INST_PATH = D_ROOT.DS.'install';

	protected static $ModuleName = '';
	protected static $ModPath = '';
	protected static $ModPathTpl = '';
	protected static $AdmPath = '';
	protected static $LangPath = '';
	protected static $ParamsURL = array();
	protected static $ControllerName = '';
	protected static $MethodName = '';
	protected static $ViewsPath = '';

	public static function ModPathTpl() {
		return self::$ModPathTpl;
	}	

	public static function LangPath() {
		return self::$LangPath;
	}
	
	public static function ModuleName() {
		return self::$ModuleName;
	}
	
	public static function AdmName() {
		$tmp1 = explode(self::DS, self::$AdmPath);
		return $tmp1[count($tmp1) - 1];
	}	

	public static function ParamsURL($indice) {
		return (self::$ParamsURL[$indice] ? self::$ParamsURL[$indice] : NULL);
	}
	
	public static function ControllerName() {
		return self::$ControllerName;
	}
	
	public static function MethodName() {
		return self::$MethodName;
	}
	
	public static function ViewsPath() {
		return self::$ViewsPath;
	}	
	
	public static function MY($arg)
	{
		switch($arg) {
				case 'HOST':
				if (self::$AdmPath != '') {
					$result = substr($_SERVER['SCRIPT_NAME'], 0, strrpos( $_SERVER['SCRIPT_NAME'], '/'));
					return '//' . $_SERVER['HTTP_HOST'] . substr($result, 0, strrpos( $result, '/'));
					
				}
				else {
					return '//' . $_SERVER['HTTP_HOST'] . substr($_SERVER['SCRIPT_NAME'], 0, strrpos( $_SERVER['SCRIPT_NAME'], '/'));
				}
				break;
				case 'ADM_HOST':
				if (self::$AdmPath != '') {
					return '//' . $_SERVER['HTTP_HOST'] . substr($_SERVER['SCRIPT_NAME'], 0, strrpos( $_SERVER['SCRIPT_NAME'], '/'));
				}
				else {
					return NULL;
				}
				break;				
				case 'PATH':
				return rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
				break;
				case 'DOMAIN':
				return $_SERVER['HTTP_HOST'];
				break;
				default:
				return NULL;
				break;
		}
	}
}