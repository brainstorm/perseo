<?php

	/*############################################
				     PerSeo CMS
			
			Copyright © 2018 BrainStorm
			https://www.per-seo.com

	*/############################################	
	
	try {
		if ((! @include_once( __DIR__.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php' )) || (!file_exists( __DIR__.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php' ))) { throw new Exception ( __DIR__.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php does not exist. Use composer to install dependencies.'); }
		@include_once( __DIR__.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php' );
	}
	catch(Exception $e) {
		die("Perseo ERROR : " . $e->getMessage());
	}
	if (defined('CRYPT_SALT')) {
		ini_set('session.save_handler', 'files');
		$key = CRYPT_SALT;
		$handler = new \PerSeo\Sessions($key);
		session_set_save_handler($handler, true);
	}
	if (ob_get_length()) { ob_end_clean(); }
	if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip'))) { ob_start('ob_gzhandler'); }
	else { ob_start(); }
	session_start();
	$secure = new \PerSeo\Secure;
	$secure->init();	
	$router = new \PerSeo\Router;
	$router->routes();
