<?php

namespace Wizard\Controllers;

use Exception;

class Test
{
	public static function main() {
			$tkname=mt_rand(0,mt_getrandmax());
			$token = \PerSeo\Secure::generate_token($tkname);
			$db = new \PerSeo\DB("mysql", \PerSeo\Request::POST('dbname', 'user'), \PerSeo\Request::POST('dbhost', 'user'), \PerSeo\Request::POST('dbuser', 'user'), \PerSeo\Request::POST('dbpass', 'pass'));
			$error = $db->err();
			if (isset($error['msg'])) {
				$token = Array(
					"CSRFName" => $tkname,
					"CSRFToken" => $token
				);			
				echo json_encode(array_merge($error, $token));
			}
			else {
				$result = Array(
					"err" => 0,
					"code" => 0,
					"msg" => "ok",
					"CSRFName" => $tkname,
					"CSRFToken" => $token
				);
				echo json_encode($result);
			}
	}
	
}