<?php

namespace Admin\Controllers;

class Start
{
	public function main()
    {		
			$mylogin = new \PerSeo\Login();
			$test = $mylogin->islogged('admins');
			if (!$test) {
				header('Location: '. \PerSeo\Path::MY('HOST').'/'. strtolower(\PerSeo\Path::ModuleName()) .'/login/');
			}
			else {
				$css = \PerSeo\Library::css('twitter-bootstrap', 'css/bootstrap.min', '3.3.7');
				$css .= \PerSeo\Library::css('font-awesome', 'css/font-awesome.min', '4.7.0');
				$css .= \PerSeo\Library::css('ionicons', 'css/ionicons.min', '4.0.0-9');
				$css .= \PerSeo\Library::css('admin-lte', 'css/AdminLTE.min', '2.4.3');
				$css .= \PerSeo\Library::css('admin-lte', 'css/skins/_all-skins.min', '2.4.3');
				$css .= \PerSeo\Library::css('morris.js', 'morris', '0.5.1');
				//$css .= \PerSeo\Library::css('jvectormap', 'jquery-jvectormap.min', '1.2.2');
				$css .= \PerSeo\Library::css('bootstrap-datepicker', 'css/bootstrap-datepicker.min', '1.7.1');
				$css .= \PerSeo\Library::css('bootstrap-daterangepicker', 'daterangepicker.min', '2.1.27');
				$css .= \PerSeo\Library::css('bootstrap3-wysiwyg', 'bootstrap3-wysihtml5.min', '0.3.3');

				$js = \PerSeo\Library::js('jquery', 'jquery.min', '3.3.1');
				$js .= \PerSeo\Library::js('jqueryui', 'jquery-ui.min', '1.12.1');
				$js .= \PerSeo\Library::js('twitter-bootstrap', 'js/bootstrap.min', '3.3.7');
				$js .= \PerSeo\Library::js('fastclick', 'fastclick.min', '1.0.6');
				$js .= \PerSeo\Library::js('admin-lte', 'js/adminlte.min', '2.4.3');
				$js .= \PerSeo\Library::js('raphael', 'raphael.min', '2.2.7');
				$js .= \PerSeo\Library::js('morris.js', 'morris.min', '0.5.1');
				$js .= \PerSeo\Library::js('jquery-sparklines', 'jquery.sparkline.min', '2.1.2');
				//$js .= \PerSeo\Library::js('jvectormap', 'jquery-jvectormap.min', '1.2.2');
				$js .= \PerSeo\Library::js('jQuery-Knob', 'jquery.knob.min', '1.2.13');
				$js .= \PerSeo\Library::js('moment.js', 'moment.min', '2.20.1');
				$js .= \PerSeo\Library::js('bootstrap-daterangepicker', 'daterangepicker.min', '2.1.27');
				$js .= \PerSeo\Library::js('bootstrap-datepicker', 'js/bootstrap-datepicker.min', '1.7.1');
				$js .= \PerSeo\Library::js('bootstrap3-wysiwyg', 'bootstrap3-wysihtml5.all.min', '0.3.3');
				$js .= \PerSeo\Library::js('jQuery-slimScroll', 'jquery.slimscroll.min', '1.3.8');
				$js .= \PerSeo\Library::js('admin-lte', 'js/pages/dashboard', '2.4.3');
				$js .= \PerSeo\Library::js('admin-lte', 'js/demo', '2.4.3');

				$vars = array(
					'title' => SITENAME,
					'username' => $mylogin->username(),
					'css' => $css,
					'js' => $js
				);
				\PerSeo\Template::show(\PerSeo\Path::ViewsPath(), 'dash.tpl', $vars, true, false);
			}
    }
}